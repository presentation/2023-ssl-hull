# About

Presentation for the Self-Supervised Learning Workshop at SFI Center of Visual Intelligence

The presentation mainly covers a light introduction to SSL, and the results of Silva and Ramírez Rivera (2022), "Representation Learning via Consistent Assignment of Views to Clusters," presented at ACM SAC'22.
